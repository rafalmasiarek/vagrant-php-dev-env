
# PHP vagrant development box provisioning with ansible

As the provisioning using the ansible provisioner is very fast you can repeat the whole procedure at any time. In order to start fresh just run vagrant destroy and vagrant up. This will undo all you manual changes done on the vagrant box and provide you with a clean setup.